const crypto = require('crypto')

const SECRET = process.env.SECRET
const ALGORITHM = process.env.ALGORITHM

const encrypt = (text) => {
    const iv = crypto.randomBytes(16)
    const cipher = crypto.createCipheriv(ALGORITHM, SECRET, iv)
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()])

    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex')
    }
}

const decrypt = (iv, content) => {
    const decipher = crypto.createDecipheriv(ALGORITHM, SECRET, Buffer.from(iv, 'hex'))
    const decrypted = Buffer.concat([decipher.update(Buffer.from(content, 'hex')), decipher.final()])
    return decrypted.toString();
}

module.exports = {
    encrypt,
    decrypt
}
