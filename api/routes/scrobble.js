var express = require('express');
var router = express.Router();
import { scrobbleTrack } from '../lastfm'
import { decryptedSessionCookie } from '../common'

const scrobble = async (req, res, next) => {
  const session = decryptedSessionCookie(req.cookies['session'])
  if (!session) {
    return res.status(304)
  }

  const response = await scrobbleTrack(req.body.track, session)
  return res.status(200).send(response)
}

router.post('/', scrobble)

module.exports = router
