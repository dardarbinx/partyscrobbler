var express = require('express');
var router = express.Router();
import { getTrackData } from '../lastfm'

const getCurrentTrack = async (req, res, next) => {
  const currentTrack = await getTrackData(req.params.user)
  if (currentTrack === null) {
    return res.status(404).send()
  }

  return res.status(200).send(currentTrack)
}

router.get('/:user', getCurrentTrack)

module.exports = router;
