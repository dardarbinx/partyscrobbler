var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
  return res.status(200).send({
    success: 'true',
    message: 'example response'
  })
})

module.exports = router;
