import { decrypt } from './crypto'

export const decryptedSessionCookie = (cookiestring) => {
  if (cookiestring == undefined) {
    return false
  }
  const parts = cookiestring.split(';')
  if (parts.length != 2) {
    return false
  }
  return decrypt(parts[0], parts[1])
}
