import { createError } from 'http-errors'
import { cors } from 'cors'
import express from 'express'
import dotenv from 'dotenv'
import cookieParser from 'cookie-parser'
// Routing
import indexRouter from './routes/index'
import currentTrackRouter from './routes/current'
import tokenRouter from './routes/token'
import scrobbleRouter from './routes/scrobble'

const NODE_ENV = process.env.NODE_ENV
const PORT = process.env.PORT
const CLIENT_URL = process.env.CLIENT_URL
const SECRET = process.env.SECRET

dotenv.config({ path: './env' })
const app = express()

// CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', CLIENT_URL)
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Credentials, Methods')
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
  next()
})

app.use(cookieParser())
app.use(express.json());

// Routes
app.use('/', indexRouter)
app.use('/current', currentTrackRouter)
app.use('/token', tokenRouter)
app.use('/scrobble', scrobbleRouter)

app.use(function(req, res, next) {
  next(createError(404))
})

app.use(function(err, req, res, next) {
  res.locals.message = err.message
  res.locals.error = 'development'
  res.status(err.status || 500)
  res.json(err.message)
})

app.listen(PORT, () => {
  console.log('listening on port ' + PORT)
  console.log('client url is ' + CLIENT_URL)
})
