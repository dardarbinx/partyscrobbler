import axios from 'axios'
import md5 from 'md5'
import * as dotenv from 'dotenv'

dotenv.config()

const LASTFM_API = process.env.LASTFM_API
const API_KEY = process.env.API_KEY
const SHARED_SECRET = process.env.SHARED_SECRET

export const getTrackData = async (user) => {
  const params = {
    api_key: API_KEY,
    method: 'user.getrecenttracks',
    user: user,
    limit: 1
  }
  const response = await get(params)
  if (response.recenttracks === undefined || response.recenttracks.track[0] === undefined) {
    return { exists: false }
  }

  const lastTrack = map(response.recenttracks.track[0])
  return lastTrack
}

export const getSession = async (token) => {
  let params = {
    api_key: API_KEY,
    method: 'auth.getSession',
    token: token
  }
  const api_sig = createSignature(params)
  const response = await get({ ...params, api_sig })
  return response.session ? {
    user: response.session.name,
    session: response.session.key
  } : {}
}

export const scrobbleTrack = async (track, session) => {
  let params = {
    api_key: API_KEY,
    artist: track.artistName,
    method: 'track.scrobble',
    sk: session,
    timestamp: track.timestamp,
    track: track.trackName
  }
  const api_sig = createSignature(params)
  const response = await post({ ...params, api_sig })
  return response
}

const get = (params) => {
  const url = createGet(params)
  console.log('GET:' + url)
  return new Promise((resolve) => axios
    .get(url)
    .then(response => resolve(response.data))
    .catch(e => {
      console.log(`GET An error occured: ${e.response.status} ${e.response.statusText} @ ${url}`)
      if (e.response.status === 403) {
        console.log('GET:Received Status 403. returning empty object')
        resolve({})
      }
    })
  )
}

const post = (params) => {
  const request = createPost(params)
  console.log('POST scrobble:' + request.body)

  return new Promise((resolve) => axios
    .post(request.url, request.body)
    .then(response => {
      resolve(response.status)
    })
    .catch(e => {
      console.log(`POST An error occured: ${e.response.status} ${e.response.statusText} @ ${request.url}`)
      resolve({})
    })
  )
}

const createGet = (params) => {
  let urlParts = []
  const keys = Object.keys(params)
  keys.forEach((key, i) => {
    if (params[key] == undefined)
      throw(`Invalid param: ${key}`)

    let part = `${key}=${params[key]}`
    urlParts.push(part)
  })
  urlParts.push('format=json')
  const queryString = urlParts.join('&')
  return LASTFM_API + '?' + queryString
}

const createPost = (params) => {
  const formBody = Object.keys(params)
    .map(key => encodeURIComponent(key + '=' + params[key]))
    .join('&')
    .replace(/%20/g, '+')
    .replace(/%3D/g, '=')

  return {
    url: `${LASTFM_API}/?format=json`,
    body: formBody
  }
}

const createSignature = (params) => {
  let signature = ''
  const keys = Object.keys(params)
  keys.forEach((key, i) => {
    signature += key + params[key]
  })
  signature += SHARED_SECRET
  return md5(signature)
}

const map = (trackData) => {
  const track = {
    trackName: trackData.name,
    artistName: trackData.artist['#text'],
    imgUrl: trackData.image[3]['#text'],
    exists: true
  }

  if (trackData["@attr"]) {
      track.playing = trackData["@attr"].nowplaying
  }

  if (trackData.date && !trackData.playing) {
    track.timestamp = trackData.date.uts
  } else {
    track.timestamp = Math.floor(Date.now() / 1000)
  }

  return track
}
