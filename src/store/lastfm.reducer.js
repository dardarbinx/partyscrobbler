import { ActionTypes } from '../enums/actions.enum'
import { trackEquals } from '../services/track'

const reducer = (state, { type, payload }) => {
  switch (type) {
    case ActionTypes.Authenticated:
      return {
        ...state,
        authenticated: payload.authenticated,
        user: payload.user
      }
    case ActionTypes.SetParty:
      return updateParty(state, payload)
    case ActionTypes.GetTrack:
      return updateTracks(state, payload)
    case ActionTypes.SetErrorMessage:
      return { ...state, error: payload } // todo: error[]
    default:
      return state
  }
}

const updateParty = (state, party) => {
  if (party === null) {
    return {
      ...state,
      party: null,
      currentTrack: null,
      previousTrack: null
    }
  }
  return { ...state, party }
}

const updateTracks = (state, currentTrack) => {
  if (state.currentTrack == null) {
    return { ...state, currentTrack }
  }
  if (trackEquals(state.currentTrack, currentTrack)) {
    return state
  }

  return {
    ...state,
    previousTrack: state.currentTrack,
    currentTrack
  }
}

export default reducer
