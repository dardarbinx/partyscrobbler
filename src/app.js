import React from 'react'
import SongDisplayer from './components/song-displayer'
import DjForm from './components/dj-form'
import Navbar from './components/navbar'
import AutomaticScrobbler from './components/automatic-scrobbler'
import './index.scss'
import { handleToken } from './services/lastfm'
import { setAuthenticated, setErrorMessage } from './store/lastfm.actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import queryString from 'query-string'

const REACT_APP_ROOT_URL = process.env.REACT_APP_ROOT_URL

export class App extends React.Component<Props> {

    componentDidMount() {
      this.props.setErrorMessage(null)
      // todo: add routing
      const qs = queryString.parse(window.location.search)
      if (qs.token) {
        handleToken(qs.token).then(response => {
          const success = response.user !== undefined
          this.props.setAuthenticated({
            authenticated: success,
            user: response.user
          })
          window.location.replace(REACT_APP_ROOT_URL)
        })
      }
    }

    render() {
        return(
          <div className="h-100 d-flex flex-column">
            <Navbar />
            <div className="flex-item container h-100 justify-content-between">
                <DjForm />
                <SongDisplayer />
                <AutomaticScrobbler />
            </div>
            <div className="container text-center">
              <span>Login won't work if you block cookies.</span>
              <br/>
              <a href="https://gitlab.com/dardarbinx/partyscrobbler" target="_blank" rel="noopener noreferrer">Gitlab repository</a>
              <br/>
              <span>obligatory "powered by <a href="http://www.last.fm" target="_blank" rel="noopener noreferrer">AudioScrobbler</a>"</span>
            </div>
          </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setAuthenticated, setErrorMessage }, dispatch)
}

export default connect(null, mapDispatchToProps)(App);
