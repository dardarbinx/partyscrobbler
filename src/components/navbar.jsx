import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setParty, setAuthenticated } from '../store/lastfm.actions'
import { connectToLastfm } from '../services/lastfm'

export class Navbar extends React.Component<Props> {

  render() {
    return(
      <nav className="navbar fixed-top navbar-light bg-light">
        <div className="container d-flex d-flex-row justify-content-between">
          <div className="flex-item">
            <a className="navbar-brand" href="/">
              {
                  this.props.party
                    ? <span className="d-none d-sm-inline">{this.props.party}'s party</span>
                    : 'Party Scrobbler'
              }
            </a>
              { this.props.party ? <button className="btn btn-link btn-xs" onClick={this.resetParty}>Leave party</button> : '' }
              </div>
              <div className="flex-item">
               { this.loginButton() }
            </div>
        </div>
      </nav>
    )
  }

  loginButton = () => {
    if (this.props.authenticated) {
      return (<span className="navbar-text">You are logged in as {this.props.user} <em className="hover-pointer" onClick={this.logout}>(clear)</em></span>)
    } else {
      return (<button className="btn btn-link btn-xs" onClick={this.login}>Login</button>)
    }
  }

  resetParty = () => {
    this.props.setParty(null)
  }

  logout = () => {
    this.props.setAuthenticated({
      authenticated: false,
      user: null,
      session: null
    })
  }

 login = () => {
   if (!this.props.authenticated) {
      connectToLastfm()
   }
  }
}

const mapStateToProps = (state, ownProps) => ({
  party: state.party,
  status: state.status,
  user: state.user,
  authenticated: state.authenticated,
  apiKey: state.apiKey,
  secret: state.secret,
  url: state.url
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setParty, setAuthenticated }, dispatch)
}

 export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
