import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setParty, setErrorMessage } from '../store/lastfm.actions'
import { ErrorTypes } from '../enums/errors.enum'

export class DjForm extends React.Component<Props> {

  constructor(props) {
    super(props)

    this.partyFieldChange = this.partyFieldChange.bind(this)
    this.submitParty = this.submitParty.bind(this)
  }

  errorMessage() {
    switch(this.props.error) {
      case (ErrorTypes.UserNotFound):
        return <span class="text-danger pb-3">This user was not found (possibly due to privacy settings).</span>
      case (ErrorTypes.NoTrack):
        return <span class="text-danger pb-3">This user hasn't scrobbled any tracks yet.</span>
      case (ErrorTypes.General):
        return <span class="text-danger pb-3">An unforeseen error occured.</span>
      default:
        return null
    }
  }

  partyFieldChange(event) {
    this.setState({ partyField: event.target.value })
    this.props.setErrorMessage(null)
  }

  submitParty(event) {
    this.props.setParty(this.state.partyField)
    event.preventDefault()
  }

  render() {
    if (this.props.party) {
        return null
    }
    return(
      <div className="container h-100 d-flex flex-column justify-content-center">
        <div>
          <p className="lead text-center">Enter a user name to start scrobbling</p>
          <p className="text-center mb-5">This user should have their <strong>recent listening</strong> setting set to true.</p>
          <form className="d-flex flex-wrap flex-column align-items-center justify-content-between" onSubmit={this.submitParty}>
            <div className="form-group flex-item">
              <input id="party" className="form-control form-control-lg" placeholder="last.fm user" type="text" onChange={this.partyFieldChange} />
            </div>
            { this.errorMessage() }
            <button className="btn btn-custom btn-lg flex-item" type="submit">
              <span role="img" aria-label="ヽ(・∀・)ﾉ">🎉</span>
              Scrobble
              <span role="img" aria-label="(๑˃ᴗ˂)ﻭ">🎸</span>
            </button>
          </form>
          <div className="flex-item"></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  party: state.party,
  apiKey: state.apiKey,
  error: state.error
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setParty, setErrorMessage }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(DjForm);
