
export const trackEquals = (track1, track2) => {
  if (track1.artistName !== track2.artistName) {
    return false
  }
  return track1.trackName === track2.trackName
}
